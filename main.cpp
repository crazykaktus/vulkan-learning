#include <iostream>
#include "VulkanBuilder.hpp"

// note for CLion - disable inspection for vulkan (it removes editor freezes)
// see https://intellij-support.jetbrains.com/hc/en-us/community/posts/207060425-Exclude-a-file-library-from-inspection



int main()
{
    vk::UniqueInstance vulkan(crazykaktus::VulkanBuilder::vulkanInstance());
    crazykaktus::VulkanBuilder::printDevices(vulkan);

    return 0;
}
