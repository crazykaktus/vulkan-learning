#pragma once

#include "application.hpp"

namespace crazykaktus
{
    class VulkanBuilder
    {
    public:
        VulkanBuilder() = delete;

        static vk::ApplicationInfo ApplicationInfoInstance()
        {
            return vk::ApplicationInfo{
                    crazykaktus::Application::NAME.c_str(),         // application name
                    crazykaktus::Application::vulkanApplicationVersion(),  // application version
                    nullptr,                 // engine name
                    crazykaktus::Application::vulkanEngineVersion(),  // engine version
                    VK_API_VERSION_1_0,      // api version
            };
        }

        static vk::UniqueInstance vulkanInstance()
        {
            return vk::createInstanceUnique(
                    vk::InstanceCreateInfo{
                            vk::InstanceCreateFlags(),  // flags
                            &(const vk::ApplicationInfo &) ApplicationInfoInstance(),
                            0,        // enabled layer count
                            nullptr,  // enabled layer names
                            0,        // enabled extension count
                            nullptr,  // enabled extension names
                    });
        }

        static void printDevices(const vk::UniqueInstance &vulkan)
        {
            std::cout << "Physical devices:" << std::endl;
            for (vk::PhysicalDevice physicalDevice : vulkan->enumeratePhysicalDevices())
            {
                std::cout << "\t" << physicalDevice.getProperties().deviceName << std::endl;
            }
        }
    };
}