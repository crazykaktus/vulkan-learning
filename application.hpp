#pragma once
#include <vulkan/vulkan.hpp>

namespace crazykaktus
{
    // see https://www.khronos.org/registry/vulkan/
    struct Application
    {
        Application() = delete;

        struct Version
        {
            Version() = delete;

            constexpr static int VARIANT = 0;
            constexpr static int MAJOR = 0;
            constexpr static int MINOR = 0;
            constexpr static int PATCH = 0;
        };


        inline const static auto NAME = std::string("01-deviceList");

        static uint32_t vulkanApplicationVersion()
        {
            return static_cast<uint32_t>(VK_MAKE_API_VERSION(
                    crazykaktus::Application::Version::VARIANT,
                    crazykaktus::Application::Version::MAJOR,
                    crazykaktus::Application::Version::MINOR,
                    crazykaktus::Application::Version::PATCH));
        }

        static uint32_t vulkanEngineVersion()
        {
            return static_cast<uint32_t>(VK_MAKE_API_VERSION(
                    crazykaktus::Application::Version::VARIANT,
                    crazykaktus::Application::Version::MAJOR,
                    crazykaktus::Application::Version::MINOR,
                    crazykaktus::Application::Version::PATCH));
        }


    };


}
