 # syntax=docker/dockerfile:1
FROM ubuntu

ARG PREP_SCRIPT=./install-vulkan.sh
WORKDIR /
COPY $PREP_SCRIPT $PREP_SCRIPT
RUN chmod +x $PREP_SCRIPT && $PREP_SCRIPT && apt clean all

