#!/bin/bash
set -e
export DEBIAN_FRONTEND=noninteractive
# clang can have install problems with Ubuntu/Mint 20.X (bad dependency)
apt update && apt install -y wget gnupg2 libx11-dev make autoconf cmake clang

wget -qO - https://packages.lunarg.com/lunarg-signing-key-pub.asc | apt-key add -
wget -qO /etc/apt/sources.list.d/lunarg-vulkan-focal.list https://packages.lunarg.com/vulkan/lunarg-vulkan-focal.list
apt update && apt install -y vulkan-sdk